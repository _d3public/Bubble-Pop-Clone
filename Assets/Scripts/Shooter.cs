using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    [SerializeField] private ShootPlaceIndicator shootPlaceIndicator;
    [SerializeField] private LineRenderer shootTrace;
    [SerializeField] private float gameAreaWidth;
    [SerializeField] private float bubbleSpeed = 10f;
    private bool isShootAvailable;
    private bool canShoot;
    private bool hasTouch = false;
    private float shootTimer=0f;
    private float shootDelay = .25f;
    private Bubble activeBubble;
    private Bubble nextBubble;
    private float halfWidth;
    private Vector3[] availablePlaces;
    private Vector3 destinationPos;
    private bool isReflected = false;
    private Vector3 reflectPosition;

    private Vector3 tPos;
    private void OnEnable()
    {
        InputManager.onInputMove += Handle_onInputMove;
        InputManager.onInputUp += Handle_onInputUp;
        InputManager.onInputDown += Handle_onInputDown;
        GameLogic.onShiftDown += GameLogic_onShiftDown;
        GameLogic.onShiftUp += GameLogic_onShiftUp;
    }
    private void OnDisable()
    {
        InputManager.onInputMove -= Handle_onInputMove;
        InputManager.onInputUp -= Handle_onInputUp;
        InputManager.onInputDown -= Handle_onInputDown;
        GameLogic.onShiftDown -= GameLogic_onShiftDown;
        GameLogic.onShiftUp -= GameLogic_onShiftUp;
    }
    private void Awake()
    {
        halfWidth = gameAreaWidth / 2f;
    }
    private void Start()
    {
        Initialize();
    }
    private void Update()
    {
        if (!canShoot)
        {
            shootTimer += Time.deltaTime;
            if (shootTimer >= shootDelay && !hasTouch)
            {
                shootTimer = 0f;
                canShoot = true;
            }
        }
    }
    void Initialize()
    {
        Prepare();
        shootTrace.gameObject.SetActive(false);
        canShoot = true;
    }
    void Prepare()
    {
        activeBubble = BubblePool.Instance.GetBubble();
        activeBubble.SetBubbleID(Random.Range(LevelManager.Instance.CurrentLevelValues.MinBubbleIndex, LevelManager.Instance.CurrentLevelValues.MaxBubbleIndex));

        var shooterPos = Vector3.zero;
        activeBubble.transform.parent = transform;
        activeBubble.transform.localPosition = shooterPos;
        activeBubble.gameObject.SetActive(true);

        PrepareNextBubble();
    }
    void PrepareNextBubble()
    {
        nextBubble = BubblePool.Instance.GetBubble();
        nextBubble.SetBubbleID(Random.Range(LevelManager.Instance.CurrentLevelValues.MinBubbleIndex, LevelManager.Instance.CurrentLevelValues.MaxBubbleIndex));

        var nextPos = Vector3.zero;
        nextPos.x -= activeBubble.BubbleSize.x;
        nextBubble.transform.parent = transform;
        nextBubble.transform.localPosition = nextPos;
        nextBubble.transform.localScale = Vector3.one * .75f;
        nextBubble.gameObject.SetActive(true);
    }
    void Shoot()
    {
        isShootAvailable = false;
        canShoot = false;
        var currentBubble = activeBubble;
        if (isReflected)
        {
            var tempReflectPos = reflectPosition;
            tempReflectPos.x += (activeBubble.BubbleSize.x / 2) * (reflectPosition.x < transform.position.x ? 1 : -1);
            currentBubble.SpeedMove(tempReflectPos, bubbleSpeed,
                () =>
                {
                    currentBubble.SpeedMove(destinationPos, bubbleSpeed,
                        () =>
                        {
                            GameLogic.Instance.AddBubble(currentBubble);
                        });
                });
        }
        else
        {
            currentBubble.SpeedMove(destinationPos, bubbleSpeed,
                        () =>
                        {
                            GameLogic.Instance.AddBubble(currentBubble);
                        });
        }
        UpdateActiveBubble();
    }
    void UpdateActiveBubble()
    {
        activeBubble = nextBubble;
        PrepareNextBubble();

        activeBubble.Scale(Vector3.one, .25f);
        activeBubble.LocalMove(Vector3.zero, .25f);
    }
    private void Handle_onInputDown()
    {
        hasTouch = true;
        if (!canShoot) return;
        availablePlaces = GameLogic.Instance.GetAvailablePlaces();
    }
    private void Handle_onInputUp()
    {
        hasTouch = false;
        if (!isShootAvailable) return;
        if (!canShoot) return;
        shootPlaceIndicator.Deactivate();
        shootTrace.gameObject.SetActive(false);
        Shoot();
    }
    private void Handle_onInputMove(Vector2 touchPoint)
    {
        if (!canShoot) return;
        tPos = touchPoint;

        Vector3 tPoint = touchPoint;
        tPoint.z = transform.position.z;
        var direction = (tPoint - transform.position) * 10f;
        var dirAngle = Mathf.Atan2(direction.normalized.y, direction.normalized.x) * Mathf.Rad2Deg;

        var opposite = Mathf.Tan(dirAngle > 90 ? (180 - dirAngle) * Mathf.Deg2Rad : dirAngle * Mathf.Deg2Rad) * halfWidth;
        var directionY = Vector3.up * opposite;
        var directionX = (dirAngle > 90 ? Vector3.left : Vector3.right) * halfWidth;
        var reflectPosition = transform.position + (directionX + directionY);
        var reflectedDirection = Vector3.Reflect(direction.normalized, directionX.normalized);

        Vector2 minMaxHeight = GameLogic.Instance.GetMinMaxHeight();
        if (reflectPosition.y >= minMaxHeight.x && reflectPosition.y <= minMaxHeight.y)
        {
            isReflected = true;
            this.reflectPosition = reflectPosition;
            Ray r = new Ray(reflectPosition, reflectedDirection);
            CheckPlaces4Shooting(r);
        }
        else if (opposite >= minMaxHeight.x)
        {
            isReflected = false;
            Ray r = new Ray(transform.position, direction);
            CheckPlaces4Shooting(r);
        }
    }
    private void CheckPlaces4Shooting(Ray shootRay)
    {
        if (availablePlaces.Length > 0)
        {
            int placeCount = availablePlaces.Length;
            bool isIntersected = false;
            for (int i = 0; i < placeCount; i++)
            {
                Vector3[] nearestBubbles = GameLogic.Instance.GetPotentialObstaclePositions(shootRay.origin, availablePlaces[i]);
                bool isIntersectAnyObstacle = false;
                if (nearestBubbles.Length > 0)
                {
                    foreach (Vector3 oP in nearestBubbles)
                        if (IsIntersectRaySphere(oP, activeBubble.BubbleSize.x * .75f, shootRay))
                        {
                            isIntersectAnyObstacle = true;
                            break;
                        }
                }
                if (!isIntersectAnyObstacle)
                    if (IsIntersectRaySphere(availablePlaces[i], activeBubble.BubbleSize.x / 2f, shootRay))
                    {
                        isIntersected = true;
                        destinationPos = availablePlaces[i];
                        shootPlaceIndicator.SetPosition(destinationPos, activeBubble.BubbleColor);

                        Vector3[] traceLinePositions;
                        if (shootRay.origin == transform.position)
                        {
                            shootTrace.positionCount = 2;
                            var direcitonMagnitude = (destinationPos - transform.position).magnitude - shootPlaceIndicator.InitialScale / 2;
                            traceLinePositions = new Vector3[]
                            {
                            transform.position,
                            transform.position + shootRay.direction * direcitonMagnitude,
                            };
                        }
                        else
                        {
                            shootTrace.positionCount = 3;
                            var direcitonMagnitude = (destinationPos - shootRay.origin).magnitude - shootPlaceIndicator.InitialScale / 2;
                            traceLinePositions = new Vector3[]
                            {
                            transform.position,
                            shootRay.origin,
                            shootRay.origin + shootRay.direction * direcitonMagnitude,
                            };
                        }
                        shootTrace.SetPositions(traceLinePositions);
                        shootTrace.gameObject.SetActive(true);
                        isShootAvailable = true;
                        break;
                    }
            }
            if (!isIntersected)
            {
                shootPlaceIndicator.Deactivate();
                shootTrace.gameObject.SetActive(false);
                isShootAvailable = false;
            }
        }
    }
    bool IsIntersectRaySphere(Vector3 center, float radius, Ray r)
    {
        Vector3 oc = r.origin - center;
        float a = Vector3.Dot(r.direction, r.direction);
        float b = 2f * Vector3.Dot(oc, r.direction);
        float c = Vector3.Dot(oc, oc) - radius * radius;
        float discriminant = b * b - 4 * a * c;
        return (discriminant > 0);
    }
    private void GameLogic_onShiftUp()
    {
        canShoot = false;
    }

    private void GameLogic_onShiftDown()
    {
        canShoot = false;
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        var direction = (tPos - transform.position) * 10f;
        Gizmos.DrawRay(transform.position, direction);
        var dirAngle = Mathf.Atan2(direction.normalized.y, direction.normalized.x) * Mathf.Rad2Deg;

        var opposite = Mathf.Tan(dirAngle > 90 ? (180 - dirAngle) * Mathf.Deg2Rad : dirAngle * Mathf.Deg2Rad) * halfWidth;
        var directionY = Vector3.up * opposite;
        var directionX = (dirAngle > 90 ? Vector3.left : Vector3.right) * halfWidth;
        var reflectPosition = transform.position + (directionX + directionY);
        Gizmos.color = Color.magenta;
        Gizmos.DrawSphere(reflectPosition, .05f);

        var reflectedDirection = Vector3.Reflect(direction.normalized, directionX.normalized);
        Gizmos.DrawRay(reflectPosition, reflectedDirection);
        Gizmos.color = Color.cyan;

        if (GameLogic.Instance)
        {
            Vector2 minMaxHeight = GameLogic.Instance.GetMinMaxHeight();
            Gizmos.color = Color.green;
            Gizmos.DrawSphere((directionX + Vector3.up * minMaxHeight.x), .05f);
            Gizmos.color = Color.cyan;
            Gizmos.DrawSphere((directionX + Vector3.up * minMaxHeight.y), .05f);
        }
    }

}
