using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameValues", menuName = "GameData/GameValues")]
public class GameValues : ScriptableObject
{
    public int BubbleCountInRow;
    public int InitialRowCount;
    public float RowXOffset;
    public float ColYOffset;
    public float OffsetBetweenBubbles;
}
