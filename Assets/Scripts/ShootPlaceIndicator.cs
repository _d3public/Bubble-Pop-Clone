using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootPlaceIndicator : MonoBehaviour
{
    internal float InitialScale => initialIndicatorScale;

    [SerializeField] private SpriteRenderer indicatorRenderer;

    private float initialIndicatorScale;
    private Coroutine scaleRoutine;
    private void Awake()
    {
        this.gameObject.SetActive(false);
        initialIndicatorScale = transform.localScale.x;
    }
    internal void SetPosition(Vector3 position, Color bubbleColor)
    {
        if (position != transform.position || !gameObject.activeSelf)
        {
            transform.position = position;
            var tempColor = bubbleColor;
            tempColor.a = .25f;
            indicatorRenderer.color = tempColor;
            Activate();
        }
    }
    internal void Activate()
    {
        transform.localScale = Vector3.zero;
        gameObject.SetActive(true);
        if (scaleRoutine != null)
            StopCoroutine(scaleRoutine);
        scaleRoutine = this.Scale(Vector3.one * initialIndicatorScale, .2f);
    }
    internal void Deactivate()
    {
        gameObject.SetActive(false);
    }
}
