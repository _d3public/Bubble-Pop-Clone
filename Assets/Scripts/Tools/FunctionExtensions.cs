using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class FunctionExtensions
{
    public static void DelayedCall(this MonoBehaviour monoBehaviour, System.Action callback = null)
    {
        monoBehaviour.StartCoroutine(DelayedCallRoutine(callback));
    }
    public static void DelayedCall(this MonoBehaviour monoBehaviour, float duration, System.Action callback = null)
    {
        monoBehaviour.StartCoroutine(DelayedCallRoutine(duration, callback));
    }
    static IEnumerator DelayedCallRoutine(float duration, System.Action callback)
    {
        yield return new WaitForSeconds(duration);
        callback?.Invoke();
    }
    static IEnumerator DelayedCallRoutine(System.Action callback)
    {
        yield return new WaitForEndOfFrame();
        callback?.Invoke();
    }
}
