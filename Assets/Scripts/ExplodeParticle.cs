using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodeParticle : MonoBehaviour, IPoolObject
{
    public ParticleSystem ParticleSystem;

    internal void ActivateParticle(Color particleColor)
    {
        ParticleSystem.MainModule mainModule = ParticleSystem.main;
        mainModule.startColor = particleColor;
        gameObject.SetActive(true);
        this.DelayedCall(.75f, () => ParticlePool.Instance.ReturnParticle(this));
    }

    public GameObject KeyObject { get; set; }
    public GameObject ToGameObject()
    {
        return this.gameObject;
    }
}
