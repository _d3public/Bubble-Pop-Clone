using System.Collections;
using System.Collections.Generic;
using TinyD3.ObjectPool;
using UnityEngine;

public class ParticlePool : MonoBehaviour
{
    public static ParticlePool Instance { get; private set; }
    private ObjectPool<ExplodeParticle> particlePool;
    public int PoolSize = 100;
    public List<GameObject> ParticlePrefabs;

    private void Awake()
    {
        Instance = this;

        CreateParticles(ParticlePrefabs);
    }

    internal void CreateParticles(List<GameObject> particles)
    {
        if (particles != null && particles.Count > 0)
        {
            if (particlePool == null)
                particlePool = new ObjectPool<ExplodeParticle>(transform, particles, PoolSize);
            else
            {
                particlePool.CreateObjects(particles, PoolSize);
            }
        }
    }

    public ExplodeParticle GetParticle(GameObject keyObject = null)
    {
        if (keyObject)
            return particlePool.GetObjectWithKey(keyObject);
        return particlePool.GetObjectWithKey(ParticlePrefabs[Random.Range(0, ParticlePrefabs.Count)]);
    }

    public void ReturnParticle(ExplodeParticle particle)
    {
        particlePool.ReturnObject(particle.KeyObject, particle);
    }
    public void ReturnAll()
    {
        particlePool.ReturnAll();
    }
}
