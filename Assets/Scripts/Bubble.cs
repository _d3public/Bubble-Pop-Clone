using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;

[SelectionBase]
public class Bubble : MonoBehaviour, IPoolObject
{
    public int BubbleID { get; private set; }
    public Vector2 BubbleSize { get; private set; }
    public Color BubbleColor { get; private set; }
    public Vector2Int Coord { get; private set; }
    public int NeighborCount { get => neighbors?.Count ?? 0; }

    [SerializeField] BubbleInfo bubbleInfo;
    [Space(10f)]
    [SerializeField] MeshFilter meshFilter;
    [SerializeField] MeshRenderer meshRenderer;
    [SerializeField] TextMeshPro numberText;
    [SerializeField] GameObject trail;


    private Rigidbody rigidbody;
    private bool isActive;
    public List<Bubble> neighbors;

    private void OnEnable()
    {
        GameLogic.onShiftDown += Handle_onShiftDown;
        GameLogic.onShiftUp += Handle_onShiftUp;
    }
    private void OnDisable()
    {
        GameLogic.onShiftDown -= Handle_onShiftDown;
        GameLogic.onShiftUp -= Handle_onShiftUp;
    }
    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
        CreateBubbleByInfo();
    }
    void CreateBubbleByInfo()
    {
        meshFilter.sharedMesh = bubbleInfo.BubbleMesh;
        var textPos = numberText.transform.localPosition;
        textPos.z = bubbleInfo.TextZValue;
        numberText.transform.localPosition = textPos;
        BubbleSize = bubbleInfo.BubbleMesh.bounds.size;
    }
    internal bool SetBubbleID(int id)
    {
        var targetID = id;
        if (id > bubbleInfo.Numbers.Length - 1)
        {
            targetID = bubbleInfo.Numbers.Length - 1;
        }
        BubbleID = targetID;
        rigidbody.isKinematic = true;
        meshRenderer.ChangeColor(bubbleInfo.Colors[targetID]);
        if (bubbleInfo.Numbers[targetID] >= 1000)
        {
            numberText.text = Mathf.CeilToInt(bubbleInfo.Numbers[targetID] / 1000).ToString() + "K";
        }
        else
        {
            numberText.text = bubbleInfo.Numbers[targetID].ToString();
        }
        BubbleColor = bubbleInfo.Colors[targetID];
        neighbors = null;
        isActive = false;
        if (targetID == bubbleInfo.Numbers.Length - 1)
            return true; // bomb activated;
        return false;
    }
    internal void SetCoord(Vector2Int coord)
    {
        Coord = coord;
    }
    internal void SetStatus(bool status)
    {
        isActive = status;
        trail.gameObject.SetActive(status);
    }
    private void CheckClusterIntegrity()
    {
        if (neighbors != null && neighbors.Count > 0)
        {
            if (Coord.x != 0)
            {
                if (neighbors.Count(n => n.transform.position.y > transform.position.y && n.isActive) == 0)
                    Fall();
            }
        }
        else
            Fall();
    }
    internal void Explode(bool removeFromController = false)
    {
        if (!removeFromController)
        {
            if (neighbors != null && neighbors.Count > 0)
            {
                foreach (Bubble b in neighbors)
                {
                    if (b.isActive)
                        b.RemoveNeighbor(this);
                }
            }


            GameLogic.Instance.RemoveBubble(this);
        }
        BubblePool.Instance.ReturnBubble(this);
    }
    internal void ActivateExplodeParticle()
    {
        var explodeParticle = ParticlePool.Instance.GetParticle();
        explodeParticle.transform.position = transform.position;
        explodeParticle.ActivateParticle(BubbleColor);
    }
    internal void Fall()
    {
        if (!isActive) return;

        var pos = transform.position;
        pos.z = .01f;
        transform.position = pos;
        rigidbody.isKinematic = false;
        rigidbody.AddForce((Random.Range(0, 2) == 0 ? Vector3.right : Vector3.left) * Random.Range(.01f, .025f), ForceMode.Impulse);
        SetStatus(false);

        if (neighbors != null && neighbors.Count > 0)
            foreach (Bubble b in neighbors)
            {
                if (b.isActive)
                    b.RemoveNeighbor(this);
            }
        GameLogic.Instance.RemoveBubble(this);
    }
    internal void AddNeighbour(Bubble newNeighbour)
    {
        if (neighbors == null)
        {
            neighbors = new List<Bubble>();
            neighbors.Add(newNeighbour);
            newNeighbour.AddNeighbour(this);
        }
        else
        {
            if (!neighbors.Contains(newNeighbour))
            {
                neighbors.Add(newNeighbour);
                newNeighbour.AddNeighbour(this);
            }
        }

    }
    internal void RemoveNeighbor(Bubble bubble)
    {
        if (neighbors != null && bubble)
        {
            if (neighbors.Contains(bubble))
            {
                neighbors.Remove(bubble);
                CheckClusterIntegrity();
            }
            else if (neighbors.Count == 0)
                Fall();
        }
        else
            Fall();
    }
    private void Handle_onShiftUp()
    {
        if (!isActive) return;
        var newPos = transform.position;
        newPos.y += BubbleSize.x + GameManager.Instance.GameValues.OffsetBetweenBubbles;
        var tempCoord = Coord;
        tempCoord.x--;
        Coord = tempCoord;
        this.TimeMove(newPos, .1f);
    }
    private void Handle_onShiftDown()
    {
        if (!isActive) return;
        var newPos = transform.position;
        newPos.y -= BubbleSize.x + GameManager.Instance.GameValues.OffsetBetweenBubbles;
        var tempCoord = Coord;
        tempCoord.x++;
        Coord = tempCoord;
        this.TimeMove(newPos, .1f);
    }
    internal List<Bubble> GetNeighbors()
    {
        return neighbors;
    }
    internal List<Bubble> GetSameIDNeighbors(List<Bubble> exceptedBubbles = null)
    {
        List<Bubble> bubbles = new List<Bubble>();
        bubbles.Add(this);
        if (exceptedBubbles != null)
        {
            if (exceptedBubbles.Count > 0)
                foreach (Bubble b in exceptedBubbles)
                {
                    if (b != this)
                        bubbles.Add(b);
                }
        }
        if (neighbors != null && neighbors.Count > 0)
            foreach (Bubble b in neighbors)
            {
                if (b.BubbleID == BubbleID)
                {
                    if (!bubbles.Contains(b))
                    {
                        bubbles.Add(b);
                        var tempChain = b.GetSameIDNeighbors(bubbles);
                        if (tempChain.Count > 0)
                            foreach (Bubble bS in tempChain)
                                if (!bubbles.Contains(bS))
                                    bubbles.Add(bS);
                    }
                }
            }
        bubbles.Remove(this);
        return bubbles;
    }
    internal void IncreaseScore()
    {
        LevelManager.Instance.AddScore(bubbleInfo.Numbers[BubbleID]);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 6)//Bottom Collider
        {
            if (!rigidbody.isKinematic)
            {
                ActivateExplodeParticle();
                Explode();
            }
        }
    }
    public GameObject KeyObject { get; set; }

    public GameObject ToGameObject()
    {
        return this.gameObject;
    }
}
