using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIResizing : MonoBehaviour
{
    public RectTransform uiTransform;
    public CanvasScaler scaler;

    private void Start()
    {
        float scale;
        float containerScale;

        scale = 1 - (uiTransform.sizeDelta.y / scaler.referenceResolution.y);
        containerScale = (uiTransform.sizeDelta.x / scaler.referenceResolution.x) + scale;

        scaler.matchWidthOrHeight += scale;
    }
}
