using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance { get; private set; }

    [SerializeField] private GameScreenUI gameScreen;
    private void Awake()
    {
        Instance = this;
    }
    internal void UpdateScore(float scoreValue)
    {
        gameScreen.SetScore(scoreValue);
    }
    internal void UpdateLevelBar(float percentage, bool immediate=false)
    {
        gameScreen.UpdateLevelBar(percentage, immediate);
    }
    internal void SetLevelInfo(int currentLevel)
    {
        gameScreen.UpdateLevelText(currentLevel);
    }
}
